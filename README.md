# Kasper2

Dieses Theme für die Ghost Blogging Plattform basiert im Ursprung auf das original Theme Casper. Es wurde modifiziert von Shorez, um der DSGVO / GDPR zu entsprechen. 
Mit Kasper2 wurde das Theme auf meine Bedürfnisse angepasst.

Folgende Änderungen wurden durchgeführt:
- anstelle Cookie Consent kommt Cookiebot zum Einsatz
- die Webfonts für Font-Awesome wurden lokalisiert 
Damit gibt es keine Inhalte mehr, die von einem externen Server geladen werden müssen.

### Excerpt from the original readme, kept for informative purposes:
> # Development 
> 
> Casper styles are compiled using Gulp/PostCSS to polyfill future CSS
> spec. You'll need Node and Gulp installed globally. After that, from
> the theme's root directory:
> 
> ```bash $ yarn install $ yarn dev ```
> 
> Now you can edit `/assets/css/` files, which will be compiled to
> `/assets/built/` automatically.
> 
> The `zip` Gulp task packages the theme files into
> `dist/<theme-name>.zip`, which you can then upload to your site.
> 
> ```bash $ yarn zip ```
> 
> # PostCSS Features Used
> 
> - Autoprefixer - Don't worry about writing browser prefixes of any kind, it's all done automatically with support for the latest 2 major
> versions of every browser.
> - Variables - Simple pure CSS variables
> - [Color Function](https://github.com/postcss/postcss-color-function)
> 
> 
> # SVG Icons
> 
> Casper uses inline SVG icons, included via Handlebars partials. You
> can find all icons inside `/partials/icons`. To use an icon just
> include the name of the relevant file, eg. To include the SVG icon in
> `/partials/icons/rss.hbs` - use `{{> "icons/rss"}}`.
> 
> You can add your own SVG icons in the same manner.


# Copyright & License
**Kasper2 Theme**: Copyright (c) 2018 netbuk - Released under the [`GPLv2`](LICENSE)
formely **Kasper Theme**: Copyright (c) 2018 shorez - Released under the [`GPLv2`](LICENSE)  
formerly **Original Casper Theme**: Copyright  (c) 2013-2018 Ghost Foundation - Released under the [`MIT license`](https://github.com/TryGhost/Casper/blob/master/LICENSE).
